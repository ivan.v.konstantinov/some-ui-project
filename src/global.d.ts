declare type Configuration = {
    api: {
        [key: string]: {
            httpMethod: "GET" | "POST",
            host: string,
            method: string
        }
    },
    links: Array<LinkType>
}

declare interface Window {
    configuration: Configuration
}

declare type LinkType = {
    name: string,
    href: string
}