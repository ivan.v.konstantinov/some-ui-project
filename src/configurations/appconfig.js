var configuration = {
    api: {
        "customer-list": {
            httpMethod: "GET",
            host: hosts["Some-Service"],
            method: "/customer/list"
        },
        "customer-byId": {
            httpMethod: "GET",
            host: hosts["Some-Service"],
            method: "/customer/{customerId}"
        },
        "customer-add": {
            httpMethod: "POST",
            host: hosts["Some-Service"],
            method: "/customer/add"
        },
        "customer-account-open-new": {
            httpMethod: "POST",
            host: hosts["Some-Service"],
            method: "/account/open-new"
        },
        "customer-account-list": {
            httpMethod: "GET",
            host: hosts["Some-Service"],
            method: "/account/list-by-customerId/{customerId}"
        },
        "account-byId" : {
            httpMethod: "GET",
            host: hosts["Some-Service"],
            method: "/account/{accountId}"
        },
        "transaction-add":{
            httpMethod: "POST",
            host: hosts["Some-Service"],
            method: "/transaction/add"
        },
        "transaction-getlist-byAccountId" : {
            httpMethod: "GET",
            host: hosts["Some-Service"],
            method: "/transaction/list-by-account/{accountId}"
        }
    },
    links: [
        {
            name: "Some API",
            href: "https://gitlab.com/ivan.v.konstantinov/some-api-project"
        },
        {
            name: "Some UI",
            href: "https://gitlab.com/ivan.v.konstantinov/some-ui-project"
        }
    ]
};

window.configuration = configuration;