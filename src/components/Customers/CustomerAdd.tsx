import React, { Component } from "react";
import { Button, Container, TextField, Typography, withStyles } from "@material-ui/core";
import { withRouter } from "react-router-dom";
import { CustomerAddStyles as styles } from "./styles";
import { AddCustomerViewModel, CustomerAddPropsType, CustomerAddStateType } from "./types";
import CustomizableSnackbar from "../Commons/CustomizableSnackbar";
import ButtonWithLoading from "../Commons/ButtonWithLoading";
import clsx from "clsx";
import AddIcon from '@material-ui/icons/Add';

class CustomerAdd extends Component<CustomerAddPropsType, CustomerAddStateType> {
    
    constructor(props: CustomerAddPropsType) {
        super(props);

        this.state = {
            customer: {
                firstName: "",
                lastName: ""
            },
            isAddLoading: false,
            isError: false,
            errorReason: ""
        }

        this.add = this.add.bind(this);
    }

    add() {
        const { history } = this.props;
        const { customer } = this.state;

        const request: AddCustomerViewModel = customer;
        const addApi = window.configuration.api["customer-add"];
        this.setState({ isAddLoading: true });

        fetch(`${addApi.host}${addApi.method}`, {
            method: addApi.httpMethod,
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(request)
        })
            .then(response => {
                if (!response.ok) {
                    throw new Error(response.statusText);
                }

                return response;
            })
            .then(() => {

                this.setState({
                    isAddLoading: false
                }, () => { history.push(`/customers`); });
            })
            .catch((reason: Error) => {
                this.setState({
                    isAddLoading: false,
                    isError: true,
                    errorReason: reason.message
                });
            });
    }

    render() {
        const { classes, history } = this.props;
        const { customer, isAddLoading, isError, errorReason } = this.state;

        return <React.Fragment>
            {
                isError && <CustomizableSnackbar severity="error" message={errorReason} closeCallback={() => { this.setState({ isError: false }) }} />
            }
            <div className={classes.header}>
                <div >
                    <Typography variant="h3" component="div" className={classes.primaryTextColor}>Some Proj</Typography>
                </div>
            </div>
            <Container fixed maxWidth="md" className={classes.container}>
                <Typography variant="h5" component="div" className={classes.primaryTextColor}>
                    Add Customer
                </Typography>
                <TextField
                    id="firstName"
                    variant="outlined"
                    label="First Name"
                    placeholder="First Name"
                    value={customer.firstName}
                    error={customer.firstName.trim() === ""}
                    onChange={(e) => { this.setState({ customer: { ...customer, firstName: e.target.value } }); }}
                />
                <TextField
                    id="lastName"
                    variant="outlined"
                    label="Last Name"
                    placeholder="Last Name"
                    value={customer.lastName}
                    error={customer.lastName.trim() === ""}
                    onChange={(e) => { this.setState({ customer: { ...customer, lastName: e.target.value } }); }}
                />
                <div className={classes.btnGroup}>
                    <ButtonWithLoading
                        startIcon={<AddIcon />}
                        variant="contained"
                        color="primary"
                        isLoading={isAddLoading}
                        disabled={isAddLoading || customer.firstName.trim() === "" || customer.lastName.trim() === ""}
                        onClick={this.add}
                        size="large"
                        className={clsx(classes.primaryBgColor, classes.addBtn)}
                    >
                        Open
                    </ButtonWithLoading>
                    <Button
                        color="primary"
                        onClick={() => { history.push(`/customers`); }}
                        size="large"
                        className={classes.primaryTextColor}
                    >
                        Cancel
                    </Button>
                </div>
            </Container>
        </React.Fragment>;
    }
}

export default withStyles(styles)(withRouter(CustomerAdd));