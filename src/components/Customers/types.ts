import { StyledComponentProps } from "@material-ui/core";
import { RouteComponentProps } from "react-router-dom";

export type CustomersPropsType = {
}
    & StyledComponentProps
    & RouteComponentProps;

export type CustomersStateType = {
    data: Array<CustomerListViewModel>,
    isError: boolean,
    errorReason: string,
    isDataLoading: boolean
};

export type CustomerListViewModel = {
    id: string,
    lastName: string,
    firstName: string
}

export type CustomerByIdPropsType = {

}
    & StyledComponentProps
    & RouteComponentProps<{ id: string }>;

export type CustomerByIdStateType = {
    customer: CustomerViewModel,
    isError: boolean,
    errorReason: string,
    isLoading: boolean
};

export type CustomerViewModel = {
    id: string,
    lastName: string,
    firstName: string,
    balance: number,
    transactions: Array<TransactionViewModel>
}

export type TransactionViewModel = {
    credit: number,
    created: Date,
    accountId: string
}

export type CustomerAccountsPropsType = {

}
    & StyledComponentProps
    & RouteComponentProps<{ id: string }>;

export type CustomerAccountsStateType = {
    data: Array<AccountViewModel>,
    isError: boolean,
    errorReason: string,
    isDataLoading: boolean
}

export type AccountViewModel = {
    id: string,
    created: Date,
    balance: number,
    transactions: Array<TransactionViewModel>
}

export type CustomerOpenAccountPropsType = {

}
    & StyledComponentProps
    & RouteComponentProps<{ id: string }>;

export type CustomerOpenAccountStateType = {
    initialCreditBuffer: string,
    isAddLoading: boolean,
    isError: boolean,
    errorReason: string
}

export type AddAccountViewModel = {
    customerId: string,
    initialCredit: number
}

export type CustomerAddPropsType = {

}
    & StyledComponentProps
    & RouteComponentProps;

export type CustomerAddStateType = {
    customer: AddCustomerViewModel,
    isAddLoading: boolean,
    isError: boolean,
    errorReason: string
};

export type AddCustomerViewModel = {
    lastName: string,
    firstName: string
}

export type CustomerAccountAddTransactionPropsType = {

}
    & StyledComponentProps
    & RouteComponentProps<{ id: string, avmid: string }>;

export type CustomerAccountAddTransactionStateType = {
    initialCreditBuffer: string,
    isAddLoading: boolean,
    isError: boolean,
    errorReason: string
}

export type AddTransactionViewModel = {
    accountId: string,
    credit: number
}

export type CustomerAccountTransactionsPropsType = {

}
    & StyledComponentProps
    & RouteComponentProps<{ id: string, avmid: string }>;

export type CustomerAccountTransactionsStateType = {
    data: Array<TransactionViewModel>,
    isError: boolean,
    errorReason: string,
    isDataLoading: boolean
}