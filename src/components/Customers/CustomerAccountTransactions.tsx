import React, { Component } from "react";
import { Button, Container, List, ListItem, ListItemText, Typography, withStyles } from "@material-ui/core";
import { withRouter } from "react-router-dom";
import { CustomerAccountTransactionsStyles as styles } from "./styles";
import { CustomerAccountTransactionsPropsType, CustomerAccountTransactionsStateType, TransactionViewModel } from "./types";
import { formatWithTime } from "../../helpers/dateFormat";
import GrayBackdrop from "../Commons/GrayBackdrop";
import CustomizableSnackbar from "../Commons/CustomizableSnackbar";
import KeyboardBackspaceIcon from '@material-ui/icons/KeyboardBackspace';

class CustomerAccountTransactions extends Component<CustomerAccountTransactionsPropsType, CustomerAccountTransactionsStateType> { 

    constructor(props: CustomerAccountTransactionsPropsType) {
        super(props);

        this.state = {
            data: [],
            isError: false,
            errorReason: "",
            isDataLoading: true
        }

        this.pullData = this.pullData.bind(this);
    }

    componentDidMount() {
        this.pullData();
    }

    pullData() {
        const listApi = window.configuration.api["transaction-getlist-byAccountId"];

        fetch(`${listApi.host}${listApi.method.replace("{accountId}", this.props.match.params.avmid || "")}`, {
            method: listApi.httpMethod
        })
            .then(response => {
                if (!response.ok) {
                    throw new Error(response.statusText);
                }

                return response.json();
            })
            .then((response: Array<TransactionViewModel>) => {
                this.setState({
                    data: response,
                    isDataLoading: false
                });
            })
            .catch((reason: Error) => {
                this.setState({
                    isError: true,
                    errorReason: reason.message,
                    isDataLoading: false
                });
            });
    }

    renderList() {
        const { classes } = this.props;
        const { data } = this.state;

        return data.length === 0
            ? <Typography component="div" variant="h5" className={classes.primaryTextColor} style={{ padding: "16px" }}>
                Add transactions so they will appear here.
            </Typography>
            : <React.Fragment>
                <div className={classes.title}>
                    <Typography variant="h4" component="div" className={classes.primaryTextColor}>
                        Transactions
                    </Typography>
                </div>
                <List className={classes.list}>
                {
                    data.map((tran, idx) => {
                        return <ListItem key={idx}>
                            <ListItemText
                                primary={`Balance: ${tran.credit}`}
                                secondary={`Created at ${formatWithTime(tran.created)}`}
                                style={{ paddingRight: "100px" }}
                            />
                        </ListItem>;
                    })
                }
                </List>
            </React.Fragment>;
    }

    render() {
        const { classes, history } = this.props;
        const { isError, errorReason, isDataLoading } = this.state;
        
        return isDataLoading
            ? <GrayBackdrop />
            : <React.Fragment>
                {
                    isError && <CustomizableSnackbar severity="error" message={errorReason} closeCallback={() => { this.setState({ isError: false }) }} />
                }
                <div className={classes.header}>
                    <div>
                        <Typography variant="h5" component="div" className={classes.primaryTextColor}>Some Proj</Typography>
                    </div>
                </div>
                <Container fixed maxWidth="md" className={classes.container}>
                    <div>
                    {
                        this.renderList()
                    }
                    </div>
                    <div className={classes.btnGroup}>
                        <Button
                            color="primary"
                            onClick={() => { history.push(`/customers/byid/${this.props.match.params.id}/accounts`); }}
                            size="large"
                            className={classes.primaryTextColor}
                            startIcon={<KeyboardBackspaceIcon />}
                        >
                            Back
                        </Button>
                    </div>
                </Container>
            </React.Fragment>
    }
}

export default withStyles(styles)(withRouter(CustomerAccountTransactions));