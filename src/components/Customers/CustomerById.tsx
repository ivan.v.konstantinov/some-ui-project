import React, { Component } from "react";
import { Button, Container, List, ListItem, ListItemSecondaryAction, ListItemText, Typography, withStyles } from "@material-ui/core";
import { withRouter } from "react-router-dom";
import { CustomerByIdStyles as styles } from "./styles";
import { CustomerByIdPropsType, CustomerByIdStateType, CustomerViewModel } from "./types";
import GrayBackdrop from "../Commons/GrayBackdrop";
import CustomizableSnackbar from "../Commons/CustomizableSnackbar";
import ListAltIcon from '@material-ui/icons/ListAlt';
import AddIcon from '@material-ui/icons/Add';
import KeyboardBackspaceIcon from '@material-ui/icons/KeyboardBackspace';
import { formatWithTime } from "../../helpers/dateFormat";

class CustomerById extends Component<CustomerByIdPropsType, CustomerByIdStateType> {
    
    constructor(props: CustomerByIdPropsType) {
        super(props);

        this.state = {
            customer: undefined,
            isError: false,
            errorReason: "",
            isLoading: true
        }

        this.pullData = this.pullData.bind(this);
        this.renderCustomer = this.renderCustomer.bind(this);
    }

    componentDidMount() {
        this.pullData();
    }

    pullData() {
        const byIdApi = window.configuration.api["customer-byId"];

        fetch(`${byIdApi.host}${byIdApi.method.replace("{customerId}", this.props.match.params.id || "")}`, {
            method: byIdApi.httpMethod
        })
            .then(response => {
                if (!response.ok) {
                    throw new Error(response.statusText);
                }

                return response.json();
            })
            .then((response: CustomerViewModel) => {
                this.setState({
                    customer: response,
                    isLoading: false
                });
            })
            .catch((reason: Error) => {
                this.setState({
                    isError: true,
                    errorReason: reason.message,
                    isLoading: false
                });
            });
    }

    renderCustomer() {
        const { classes } = this.props;
        const { customer } = this.state;

        return <React.Fragment>
            <div className={classes.title}>
                <Typography variant="h4" component="div" className={classes.primaryTextColor}>
                    {customer.lastName} {customer.firstName}
                </Typography>
                <Typography variant="h5" component="div" className={classes.primaryTextColor}>
                    Balance: {customer.balance}
                </Typography>
            </div>
            <List>
                {
                    customer.transactions.map((trans, idx) => {
                        return <React.Fragment key={idx}>
                            <ListItem>
                                <ListItemText
                                    primary={trans.credit}
                                    secondary={formatWithTime(trans.created)}
                                />
                                <ListItemSecondaryAction className={classes.listItemAction}>
                                    <Typography className={classes.listDate} variant="caption" component="div">
                                        AccId: {trans.accountId}
                                    </Typography>
                                </ListItemSecondaryAction>
                            </ListItem>
                        </React.Fragment>
                    })
                }
            </List>
        </React.Fragment>
    }

    render() {
        const { classes, history } = this.props;
        const { isError, errorReason, isLoading, customer } = this.state;

        return isLoading
            ? <GrayBackdrop />
            : <React.Fragment>
                {
                    isError && <CustomizableSnackbar severity="error" message={errorReason} closeCallback={() => { this.setState({ isError: false }) }} />
                }
                <div className={classes.header}>
                    <div>
                        <Typography variant="h5" component="div" className={classes.primaryTextColor}>Some Proj</Typography>
                    </div>
                    <div>
                        <Button
                            color="primary"
                            onClick={() => { history.push(`/customers/byid/${customer.id}/accounts`); }}
                            size="large"
                            className={classes.primaryTextColor}
                            startIcon={<ListAltIcon />}
                        >
                            Accounts
                        </Button>
                        <Button
                            color="primary"
                            onClick={() => { history.push(`/customers/byid/${customer.id}/accounts/add`); }}
                            size="large"
                            className={classes.primaryTextColor}
                            startIcon={<AddIcon />}
                        >
                            Open New Account
                        </Button>
                    </div>
                </div>
                <Container fixed maxWidth="md" className={classes.container}>
                    <div>
                        {
                            this.renderCustomer()
                        }
                    </div>
                    <div className={classes.btnGroup}>
                        <Button
                            color="primary"
                            onClick={() => { history.push("/customers"); }}
                            size="large"
                            className={classes.primaryTextColor}
                            startIcon={<KeyboardBackspaceIcon />}
                        >
                            Back
                        </Button>
                    </div>
                </Container>
            </React.Fragment>
    }
}

export default withStyles(styles)(withRouter(CustomerById));