import React, { Component } from "react";
import { AddTransactionViewModel, CustomerAccountAddTransactionPropsType, CustomerAccountAddTransactionStateType } from "./types";
import { Button, Container, TextField, Typography, withStyles } from "@material-ui/core";
import { withRouter } from "react-router-dom";
import { CustomerAccountAddTransactionStyles as styles } from "./styles";
import CustomizableSnackbar from "../Commons/CustomizableSnackbar";
import ButtonWithLoading from "../Commons/ButtonWithLoading";
import clsx from "clsx";
import AddIcon from '@material-ui/icons/Add';

class CustomerAccountAddTransaction extends Component<CustomerAccountAddTransactionPropsType, CustomerAccountAddTransactionStateType> {
    
    constructor(props: CustomerAccountAddTransactionPropsType) {
        super(props);

        this.state = {
            initialCreditBuffer: "",
            isAddLoading: false,
            isError: false,
            errorReason: ""
        }

        this.add = this.add.bind(this);
    }

    add() {
        const { history } = this.props;
        const { initialCreditBuffer } = this.state;

        let initialCreditIsCorrect = /^-?\d+\.?\d*$/.test(initialCreditBuffer);

        if (!initialCreditIsCorrect) {
            this.setState({
                isAddLoading: false,
                isError: true,
                errorReason: "Incorrect Credit Value"
            });
        }

        let request: AddTransactionViewModel = {
            accountId: this.props.match.params.avmid,
            credit: parseFloat(initialCreditBuffer)
        };

        const addApi = window.configuration.api["transaction-add"];
        this.setState({ isAddLoading: true });

        fetch(`${addApi.host}${addApi.method}`, {
            method: addApi.httpMethod,
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(request)
        })
            .then(response => {
                if (!response.ok) {
                    throw new Error(response.statusText);
                }

                return response;
            })
            .then(() => {

                this.setState({
                    isAddLoading: false
                }, () => { history.push(`/customers/byid/${this.props.match.params.id}/accounts`); });
            })
            .catch((reason: Error) => {
                this.setState({
                    isAddLoading: false,
                    isError: true,
                    errorReason: reason.message
                });
            });
    }

    render() {
        const { classes, history } = this.props;
        const { initialCreditBuffer, isAddLoading, isError, errorReason } = this.state;

        return <React.Fragment>
            {
                isError && <CustomizableSnackbar severity="error" message={errorReason} closeCallback={() => { this.setState({ isError: false }) }} />
            }
            <div className={classes.header}>
                <div >
                    <Typography variant="h5" component="div" className={classes.primaryTextColor}>Some Proj</Typography>
                </div>
            </div>
            <Container fixed maxWidth="md" className={classes.container}>
                <Typography variant="h5" component="div" className={classes.primaryTextColor}>
                    {`Add transaction to Account ${this.props.match.params.avmid}`}
                </Typography>
                <TextField
                    id="initialCredit"
                    variant="outlined"
                    label="Initial Credit"
                    placeholder="Initial Credit"
                    value={initialCreditBuffer}
                    error={initialCreditBuffer === "0" || initialCreditBuffer === "-0"}
                    onChange={(e) => { /^-?\d+\.?\d*$/.test(e.target.value) && this.setState({ initialCreditBuffer: e.target.value } ); }}
                />
                <div className={classes.btnGroup}>
                    <ButtonWithLoading
                        startIcon={<AddIcon />}
                        variant="contained"
                        color="primary"
                        isLoading={isAddLoading}
                        disabled={isAddLoading || initialCreditBuffer === "0" || initialCreditBuffer === "-0"}
                        onClick={this.add}
                        size="large"
                        className={clsx(classes.primaryBgColor, classes.addBtn)}
                    >
                        Add
                    </ButtonWithLoading>
                    <Button
                        color="primary"
                        onClick={() => { history.push(`/customers/byid/${this.props.match.params.id}/accounts`); }}
                        size="large"
                        className={classes.primaryTextColor}
                    >
                        Cancel
                    </Button>
                </div>
            </Container>
        </React.Fragment>;
    }
}

export default withStyles(styles)(withRouter(CustomerAccountAddTransaction));