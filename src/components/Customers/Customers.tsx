import React, { Component,  } from "react";
import { Button, Container, IconButton, List, ListItem, ListItemSecondaryAction, ListItemText, Typography, withStyles } from "@material-ui/core";
import { withRouter } from "react-router-dom";
import { CustomersStyles as styles } from "./styles";
import { CustomerListViewModel, CustomersPropsType, CustomersStateType } from "./types";
import PageviewIcon from '@material-ui/icons/Pageview';
import AddIcon from '@material-ui/icons/Add';
import GrayBackdrop from "../Commons/GrayBackdrop";
import CustomizableSnackbar from "../Commons/CustomizableSnackbar";

class Customers extends Component<CustomersPropsType, CustomersStateType> {

    constructor(props: CustomersPropsType) {
        super(props);

        this.state = {
            data: [],
            isError: false,
            errorReason: "",
            isDataLoading: true
        }

        this.pullData = this.pullData.bind(this);
    }

    componentDidMount() {
        this.pullData();
    }

    pullData() {
        const listApi = window.configuration.api["customer-list"];

        fetch(`${listApi.host}${listApi.method}`, { method: listApi.httpMethod })
            .then(response => {
                if (!response.ok) {
                    throw new Error(response.statusText);
                }

                return response.json();
            })
            .then((response: Array<CustomerListViewModel>) => {
                this.setState({
                    data: response,
                    isDataLoading: false
                });
            })
            .catch((reason: Error) => {
                this.setState({
                    isError: true,
                    errorReason: reason.message,
                    isDataLoading: false
                });
            });
    }

    renderList() {
        const { classes, history } = this.props;
        const { data } = this.state;

        return data.length === 0
            ? <Typography component="div" variant="h5" className={classes.primaryTextColor} style={{ padding: "16px" }}>
                Add customers so they will appear here.
            </Typography>
            : <React.Fragment>
                <div className={classes.title}>
                    <Typography variant="h4" component="div" className={classes.primaryTextColor}>
                        Customers
                    </Typography>
                </div>
                <List className={classes.list}>
                    {
                        data.map((d, idx) => {
                            return <ListItem key={idx}>
                                <ListItemText
                                    primary={`${d.lastName} ${d.firstName}`}
                                    secondary={d.id }
                                    style={{ paddingRight: "100px" }}
                                />
                                <ListItemSecondaryAction className={classes.listItemAction}>
                                    <IconButton edge="end" onClick={() => { history.push(`/customers/byid/${d.id}`) }}>
                                        <PageviewIcon fontSize="large" className={classes.successTextColor} />
                                    </IconButton>
                                </ListItemSecondaryAction>
                            </ListItem>;
                        })
                    }
                </List>
            </React.Fragment>;
    }

    render() {
        const { classes, history } = this.props;
        const { isError, errorReason, isDataLoading } = this.state;
        
        return isDataLoading
            ? <GrayBackdrop />
            : <React.Fragment>
                {
                    isError && <CustomizableSnackbar severity="error" message={errorReason} closeCallback={() => { this.setState({ isError: false }) }} />
                }
                <div className={classes.header}>
                    <div>
                        <Typography variant="h5" component="div" className={classes.primaryTextColor}>Some Proj</Typography>
                    </div>
                    <div>
                        <Button
                            color="primary"
                            onClick={() => { history.push("/customers/add"); }}
                            size="large"
                            className={classes.primaryTextColor}
                            startIcon={<AddIcon />}
                        >
                            Add Customer
                        </Button>
                    </div>
                </div>
                <Container fixed maxWidth="md" className={classes.container}>
                    <div>
                    {
                        this.renderList()
                    }
                    </div>
                </Container>
            </React.Fragment>
    }
}

export default withStyles(styles)(withRouter(Customers));