import { StyleRules, Theme } from "@material-ui/core";

const CustomersStyles = (theme: Theme): StyleRules => ({
    header: {
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        padding: theme.spacing(2)
    },
    container: {
        paddingLeft: 0,
        paddingRight: 0
    },
    list: {
        "& > li": {
            borderBottom: "1px solid #D8D8D8"
        },
        "& > li:last-child": {
            borderBottom: "0px"
        }
    },
    listItemAction: {
        display: "flex",
        alignItems: "center"
    },
    successTextColor: {
        color: "#2CAD17"
    },
    primaryTextColor: {
        color: "#0D386C"
    },
    title: {
        padding: theme.spacing(2)
    }
});

const CustomerByIdStyles = (theme: Theme): StyleRules => ({
    header: {
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        padding: theme.spacing(2)
    },
    container: {
        paddingLeft: 0,
        paddingRight: 0
    },
    listItemAction: {
        display: "flex",
        alignItems: "center"
    },
    primaryTextColor: {
        color: "#0D386C"
    },
    btnGroup: {
        display: "flex",
        justifyContent: "space-between",
        padding: theme.spacing(2)
    },
    title: {
        padding: theme.spacing(2)
    }
});

const CustomerAccounts = (theme: Theme): StyleRules => ({
    header: {
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        padding: theme.spacing(2)
    },
    container: {
        paddingLeft: 0,
        paddingRight: 0
    },
    list: {
        "& > li": {
            borderBottom: "1px solid #D8D8D8"
        },
        "& > li:last-child": {
            borderBottom: "0px"
        }
    },
    listItemAction: {
        display: "flex",
        alignItems: "center"
    },
    successTextColor: {
        color: "#2CAD17"
    },
    primaryTextColor: {
        color: "#0D386C"
    },
    title: {
        padding: theme.spacing(2)
    }
});

const CustomerAddAccountStyles = (theme: Theme): StyleRules => ({
    header: {
        display: "flex",
        justifyContent: "flex-start",
        alignItems: "center",
        padding: theme.spacing(2)
    },
    groupButton: {

    },
    container: {
        display: "flex",
        flexDirection: "column",
        "& > *": {
            marginBottom: theme.spacing(2)
        }
    },
    btnGroup: {
        display: "flex",
        justifyContent: "space-between"
    },
    primaryTextColor: {
        color: "#0D386C"
    },
    primaryBgColor: {
        backgroundColor: "#0D386C",
        color: "#fff"
    },
    addBtn: {
        "&:hover": {
            backgroundColor: "rgba(13, 56, 108, 0.75)",
        }
    }
});

const CustomerAddStyles = (theme: Theme): StyleRules => ({
    header: {
        display: "flex",
        justifyContent: "flex-start",
        alignItems: "center",
        padding: theme.spacing(2)
    },
    groupButton: {

    },
    container: {
        display: "flex",
        flexDirection: "column",
        "& > *": {
            marginBottom: theme.spacing(2)
        }
    },
    btnGroup: {
        display: "flex",
        justifyContent: "space-between"
    },
    primaryTextColor: {
        color: "#0D386C"
    },
    primaryBgColor: {
        backgroundColor: "#0D386C",
        color: "#fff"
    },
    addBtn: {
        "&:hover": {
            backgroundColor: "rgba(13, 56, 108, 0.75)",
        }
    }
});

const CustomerAccountAddTransactionStyles = (theme: Theme): StyleRules => ({
    header: {
        display: "flex",
        justifyContent: "flex-start",
        alignItems: "center",
        padding: theme.spacing(2)
    },
    groupButton: {

    },
    container: {
        display: "flex",
        flexDirection: "column",
        "& > *": {
            marginBottom: theme.spacing(2)
        }
    },
    btnGroup: {
        display: "flex",
        justifyContent: "space-between"
    },
    primaryTextColor: {
        color: "#0D386C"
    },
    primaryBgColor: {
        backgroundColor: "#0D386C",
        color: "#fff"
    },
    addBtn: {
        "&:hover": {
            backgroundColor: "rgba(13, 56, 108, 0.75)",
        }
    }
});

const CustomerAccountTransactionsStyles = (theme: Theme): StyleRules => ({
    header: {
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        padding: theme.spacing(2)
    },
    container: {
        paddingLeft: 0,
        paddingRight: 0
    },
    list: {
        "& > li": {
            borderBottom: "1px solid #D8D8D8"
        },
        "& > li:last-child": {
            borderBottom: "0px"
        }
    },
    listItemAction: {
        display: "flex",
        alignItems: "center"
    },
    successTextColor: {
        color: "#2CAD17"
    },
    primaryTextColor: {
        color: "#0D386C"
    },
    title: {
        padding: theme.spacing(2)
    }
});

export { CustomersStyles, CustomerByIdStyles, CustomerAccounts, CustomerAddAccountStyles, CustomerAddStyles, CustomerAccountAddTransactionStyles, CustomerAccountTransactionsStyles };