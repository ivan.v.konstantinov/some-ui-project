import React, { Component } from "react";
import { Button, Container, IconButton, List, ListItem, ListItemSecondaryAction, ListItemText, Typography, withStyles } from "@material-ui/core";
import { withRouter } from "react-router-dom";
import { CustomerAccounts as styles } from "./styles";
import { AccountViewModel, CustomerAccountsPropsType, CustomerAccountsStateType } from "./types";
import AddIcon from '@material-ui/icons/Add';
import PageviewIcon from '@material-ui/icons/Pageview';
import KeyboardBackspaceIcon from '@material-ui/icons/KeyboardBackspace';
import GrayBackdrop from "../Commons/GrayBackdrop";
import CustomizableSnackbar from "../Commons/CustomizableSnackbar";
import { formatWithTime } from "../../helpers/dateFormat";

class CustomerAccounts extends Component<CustomerAccountsPropsType, CustomerAccountsStateType> { 
    constructor(props: CustomerAccountsPropsType) {
        super(props);

        this.state = {
            data: [],
            isError: false,
            errorReason: "",
            isDataLoading: true
        }

        this.pullData = this.pullData.bind(this);
    }

    componentDidMount() {
        this.pullData();
    }

    pullData() {
        const listApi = window.configuration.api["customer-account-list"];

        fetch(`${listApi.host}${listApi.method.replace("{customerId}", this.props.match.params.id || "")}`, {
            method: listApi.httpMethod
        })
            .then(response => {
                if (!response.ok) {
                    throw new Error(response.statusText);
                }

                return response.json();
            })
            .then((response: Array<AccountViewModel>) => {
                this.setState({
                    data: response,
                    isDataLoading: false
                });
            })
            .catch((reason: Error) => {
                this.setState({
                    isError: true,
                    errorReason: reason.message,
                    isDataLoading: false
                });
            });
    }

    renderList() {
        const { classes, history } = this.props;
        const { data } = this.state;

        return data.length === 0
            ? <Typography component="div" variant="h5" className={classes.primaryTextColor} style={{ padding: "16px" }}>
                Open new accounts so they will appear here.
            </Typography>
            : <React.Fragment>
                <div className={classes.title}>
                    <Typography variant="h4" component="div" className={classes.primaryTextColor}>
                        Accounts
                    </Typography>
                </div>
                <List className={classes.list}>
                {
                    data.map((avm, idx) => {
                        return <ListItem key={idx}>
                            <ListItemText
                                primary={`Balance: ${avm.balance}`}
                                secondary={`Created at ${formatWithTime(avm.created)}`}
                                style={{ paddingRight: "100px" }}
                            />
                            <ListItemSecondaryAction className={classes.listItemAction}>
                                <Typography className={classes.listDate} variant="caption" component="div">
                                        AccId: {avm.id}
                                </Typography>
                                <IconButton edge="end" onClick={() => { history.push(`/customers/byid/${this.props.match.params.id}/accounts/${avm.id}/add-transaction`) }}>
                                    <AddIcon fontSize="large" className={classes.successTextColor} />
                                </IconButton>
                                <IconButton edge="end" onClick={() => { history.push(`/customers/byid/${this.props.match.params.id}/accounts/${avm.id}`) }}>
                                    <PageviewIcon fontSize="large" className={classes.successTextColor} />
                                </IconButton>
                            </ListItemSecondaryAction>
                        </ListItem>;
                    })
                }
                </List>
            </React.Fragment>
             ;
    }

    render() {
        const { classes, history } = this.props;
        const { isError, errorReason, isDataLoading } = this.state;
        
        return isDataLoading
            ? <GrayBackdrop />
            : <React.Fragment>
                {
                    isError && <CustomizableSnackbar severity="error" message={errorReason} closeCallback={() => { this.setState({ isError: false }) }} />
                }
                <div className={classes.header}>
                    <div>
                        <Typography variant="h5" component="div" className={classes.primaryTextColor}>Some Proj</Typography>
                    </div>
                </div>
                <Container fixed maxWidth="md" className={classes.container}>
                    <div>
                    {
                        this.renderList()
                    }
                    </div>
                    <div className={classes.btnGroup}>
                        <Button
                            color="primary"
                            onClick={() => { history.push(`/customers/byid/${this.props.match.params.id}`); }}
                            size="large"
                            className={classes.primaryTextColor}
                            startIcon={<KeyboardBackspaceIcon />}
                        >
                            Back
                        </Button>
                    </div>
                </Container>
            </React.Fragment>
    }

}

export default withStyles(styles)(withRouter(CustomerAccounts));