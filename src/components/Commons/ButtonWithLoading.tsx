import React, { Component } from 'react';
import { Theme, StyleRules, withStyles, StyledComponentProps } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import Button, { ButtonProps } from '@material-ui/core/Button';

const styles = (theme: Theme): StyleRules => ({
    wrapper: {
        position: "relative",
        display: "inline-block"
    },
    buttonProgress: {
        position: "absolute",
        top: "50%",
        left: "50%",
        marginTop: -12,
        marginLeft: -12,
    }
});

export type ButtonWithLoadingPropsType = {
    isLoading: boolean
}
    & ButtonProps
    & StyledComponentProps;

export type ButtonWithLoadingStateType = {
    isLoading: boolean
}

class ButtonWithLoading extends Component<ButtonWithLoadingPropsType, ButtonWithLoadingStateType> {

    constructor(props: ButtonWithLoadingPropsType) {
        super(props);

        const { isLoading } = this.props;

        this.state = { isLoading };
    }

    componentWillReceiveProps(newProps: ButtonWithLoadingPropsType) {
        this.setState({
            isLoading: newProps.isLoading
        })
    }

    render() {
        const { classes, isLoading, ...otherProps } = this.props;

        return (
            <div className={classes.wrapper} style={{ width: this.props.fullWidth ? "100%" : "auto" }}>
                <Button {...otherProps}>{otherProps.children}</Button>
                {
                    this.state.isLoading && <CircularProgress size={24} className={classes.buttonProgress} />
                }
            </div>
        );
    }
};

export default withStyles(styles)(ButtonWithLoading);