import React from 'react';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        backdrop: {
            zIndex: 1000,
            color: theme.palette.primary.main,
            backgroundColor: "rgba(#F2F2F2, 0.1)"
        },
    }),
);

export default function GrayBackdrop() {
    const classes = useStyles();

    return <Backdrop className={classes.backdrop} open={true}>
        <CircularProgress color="inherit" />
    </Backdrop>;
}