import React from 'react';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert, { AlertProps, Color } from '@material-ui/lab/Alert';

function Alert(props: AlertProps) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export type CustomizableSnackbarPropsType = {
    severity: Color,
    message: string,
    closeCallback?: () => void
}

export default (props: CustomizableSnackbarPropsType) => {
    const [open, setOpen] = React.useState(true);

    const handleClose = (event?: React.SyntheticEvent, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }
        
        setOpen(false);
        props.closeCallback && props.closeCallback();
    };

    return <Snackbar open={open} autoHideDuration={6000} onClose={handleClose} anchorOrigin={{ vertical: "top", horizontal: "center" }}>
        <Alert onClose={handleClose} severity={props.severity}>
            {props.message}
        </Alert>
    </Snackbar>
}