import { StyledComponentProps } from "@material-ui/core";
import { RouteComponentProps } from "react-router-dom";

export type MainAppPropsType = {
}
& StyledComponentProps
& RouteComponentProps;

export type MainAppStateType = { 
}