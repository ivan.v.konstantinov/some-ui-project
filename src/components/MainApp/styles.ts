import { Theme, StyleRules } from '@material-ui/core/styles';

export const styles = (theme: Theme): StyleRules => ({
  '@global': {
    '*::-webkit-scrollbar': {
      width: '5px'
    },
    '*::-webkit-scrollbar-track': {
      '-webkit-box-shadow': 'inset 0 0 6px rgba(0,0,0,0.00)'
    },
    '*::-webkit-scrollbar-thumb': {
      backgroundColor: 'rgb(13,56,108)',
      outline: '1px solid rgba(13,56,108, 0.05)'
    },
    "html, body, #container": {
      height: "100%"
    }
  },
  footer: {
    padding: `${theme.spacing(3)}px ${theme.spacing(2)}px`,
    display: "flex",
    height: "50px",
    justifyContent: "center",
    alignItems: "center",
    alignContent: "space-around",
    "& > a": {
      padding: "0px 16px"
    },
    backgroundColor: "#fff",
    boxSizing: "border-box"
  },
  link: {
    color: "#0D386C !important",
    fontFamily: "Roboto",
    fontSize: "12px"
  },
  mainAppContainer: {
    height: "100%",
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between"
  }
});