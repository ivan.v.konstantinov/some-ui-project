import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { styles } from "./styles"
import { Redirect, Route, Switch, withRouter } from "react-router-dom";
import { MainAppPropsType, MainAppStateType } from './types';
import { CustomerAccountTransactions, CustomerAccountAddTransaction, CustomerAdd, CustomerOpenAccount, CustomerAccounts, CustomerById, Customers } from '../Customers';

class MainApp extends Component<MainAppPropsType, MainAppStateType> {

  constructor(props: MainAppPropsType) {
    super(props);

  }

  render() {
    const { classes } = this.props;

    return <div className={classes.mainAppContainer}>
      <div className={classes.switch}>
        <Switch>
          <Route path="/" exact render={(props) => { return <Redirect to="/customers" /> }} />
          <Route path="/customers/byid/:id/accounts/add" component={CustomerOpenAccount} />
          <Route path="/customers/byid/:id/accounts/:avmid/add-transaction" component={CustomerAccountAddTransaction} />
          <Route path="/customers/byid/:id/accounts/:avmid" component={CustomerAccountTransactions} />
          <Route path="/customers/byid/:id/accounts" component={CustomerAccounts} />
          <Route path="/customers/byid/:id" component={CustomerById} />
          <Route path="/customers/add" component={CustomerAdd} />
          <Route path="/customers" component={Customers} />
        </Switch>

      </div>
      <div className={classes.footer}>
        {
          window.configuration.links.map((l, idx) => {
            return <a key={idx} href={l.href} target="_blank" className={classes.link} >{l.name}</a>
          })
        }
      </div>
    </div>
  }
}

export default withStyles(styles)(withRouter(MainApp));