import format from "date-fns/format";
import ru from "date-fns/locale/ru";

const  formatWithTime = (value: string | number| Date) => format(new Date(value), "dd.MM.yyyy HH:mm:ss", { locale: ru });

export { formatWithTime }