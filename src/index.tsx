import React from "react"
import { render } from "react-dom"
import { MainApp } from "./components/MainApp"
import { HashRouter as Router, Route } from "react-router-dom"

render(
    <Router>
        <Route component={MainApp} />
    </Router>,
    document.getElementById("container")
);