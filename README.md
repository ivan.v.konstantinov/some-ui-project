# Some UI Project

Test project to check if I can do code on React, set CI-CD on gitlab and show it to the world.

# How to run the project

Before you run this project make sure you have run that project → [Some API Project](https://gitlab.com/ivan.v.konstantinov/some-api-project)
_This_project_doesn't_work_separately_from_API_project!_

0. Install VS Code - [Visual Studio Code](https://code.visualstudio.com/)
1. Clone the repo on your machine
2. Right click on project folder and 'Start with Code'
3. After you have seen that message 'Loading TS/JS language' has disappeared → menu Terminal  → New Terminal
4. In the Terminal console print: npm install
5. Press Enter
6. If it displayed you to run 'npm fund' or 'npm audit' after the installation of all the packages - run the command
7. In terminal console print: npm run start
8. Press Enter
9. Wait for it

In theory after 8th step you have to see minimalistic UI with 3 names of predefined Customers.
Afterwards you are free to modify data as you wish. But remember once you have shut down Some API Project the data are lost - read about it in API repo README.

# How it works

It's a simple React-application that communicates with 'Some API' services to show you how two applications work together: UI and Services.