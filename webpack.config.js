const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const outputPath = path.resolve(__dirname, "./dist");
const WebpackVersionFilePlugin = require("webpack-version-file-plugin");
const execa = require('execa');
var HtmlWebpackTagsPlugin = require("html-webpack-tags-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");

module.exports = {
    entry: {
        app: [
            path.resolve(__dirname, "./src/index.tsx")
        ]
    },
    output: {
        path: path.resolve(__dirname, './dist'),
        filename: '[name].js'
    },
    mode: "development",
    devtool: "source-map",
    resolve: {
        extensions: [".ts", ".tsx", ".js", ".json"]
    },
    module: {
        rules: [
            {
                test: /\.ts(x?)$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: "ts-loader"
                    }
                ]
            },
            {
                test: /\.(gif|png|jpg|jpeg|svg)$/,
                exclude: /node_modules/,
                include: path.resolve(__dirname, "./src/assets/"),
                use: [
                    {
                        loader: "url-loader?limit=10000"
                    }
                ]
            },
            {
                enforce: "pre",
                test: /\.js$/,
                loader: "source-map-loader"
            }
        ]
    },
    plugins: [
        new CopyWebpackPlugin([
            { from: "src/configurations", to: "configurations" }
        ]),
        new HtmlWebpackPlugin({
            template: path.join(__dirname, "./src/assets/index.html"),
            filename: "index.html",
            path: outputPath
        }),
        new HtmlWebpackTagsPlugin({ scripts: ["configurations/hosts.js", "configurations/appconfig.js"], append: false })       
    ],
    devServer: {
        contentBase: "./dist",
        host: "localhost",
        port: 8100,
        hot: true,
        inline: true,
        historyApiFallback: true,
        open: true
    }
}